#include <cs50.h>
#include <stdio.h>
#include <math.h>

int main(void)
{
    //Taking input
    long long cc_input = get_long_long("Tell your credit card no: ");

    //copying input value for reason
    long long cc_dgt_sum = cc_input;
    long long cc_company = cc_input;

    //checking for digits
    int n = 0, divi;
    do
    {
        divi = cc_input % 10;
        n++;
        cc_input = cc_input / 10;
    }
    while (cc_input != 0);

    //finding sum of digits
    int pro = 1, sum = 0, sum2, dgt, dgt1, dgt2, dgt3, dgt4, dgt5;

    //for a bit settlement using if-else
    int first_two_dgt, first_dgt;
    first_two_dgt = cc_company / pow(10, (n - 2));
    first_dgt = cc_company / pow(10, (n - 1));
    if (first_two_dgt == 55 || first_two_dgt == 51)
    {

        for (int i = 0; i < n; i++)
        {
            //digits which aren't multiplied by 2
            dgt = cc_dgt_sum % 10;
            sum = sum + dgt;
            //singlising the sum if it gets of 2 duguts
            if (sum % 10 != 0)
            {
                dgt4 = sum % 10;
                dgt5 = sum / 10;
                sum = sum + dgt4 + dgt5;
            }


            //these are the digits multiplied by 2
            cc_dgt_sum = cc_dgt_sum / 10;
            dgt1 = cc_dgt_sum % 10;

            pro = dgt1 * 2;
            if (pro % 10 != 0)
            {
                dgt2 = pro % 10;
                dgt3 = pro / 10;

                sum = sum + dgt2 + dgt3;
            }
            else
            {
                sum = sum + pro;
            }
            sum2 = sum;
            cc_dgt_sum = cc_dgt_sum / 10;
        }

    }
    else
    {

        for (int i = 0; i < n; i++)
        {
            //digits which aren't multiplied by 2
            dgt = cc_dgt_sum % 10;
            sum = sum + dgt;

            //these are the digits multiplied by 2
            cc_dgt_sum = cc_dgt_sum / 10;
            dgt1 = cc_dgt_sum % 10;

            pro = dgt1 * 2;
            if (pro % 10 != 0)
            {
                dgt2 = pro % 10;
                dgt3 = pro / 10;

                sum = sum + dgt2 + dgt3;
            }
            else
            {
                sum = sum + pro;
            }
            sum2 = sum;
            cc_dgt_sum = cc_dgt_sum / 10;
        }

    }
    //Checking for company of card
    if (sum2 % 10 != 0)//using sum cheking vaidity
    {
        printf("INVALID\n");
    }
    else //finding first two digits of cc_input
    {
        first_two_dgt = cc_company / pow(10, (n - 2));
        first_dgt = cc_company / pow(10, (n - 1));
        if (n == 15)//for Amex
        {
            switch (first_two_dgt)
            {
                case 34:
                case 37:
                    printf("AMEX\n");
                    break;
                default:
                    printf("INVALID\n");
            }
        }
        else if (n == 16)//for Visa & Master
        {
            if (first_dgt == 4)
            {
                printf("VISA\n");
            }
            if (first_two_dgt > 50 && first_two_dgt < 56)
            {
                printf("MASTERCARD\n");
            }
        }
        else if (n == 13 && first_dgt == 4)//for Visa
        {
            printf("VISA\n");
        }
        else //It will eliminate the the fault of sum varification
        {
            printf("INVALID\n");
        }

    }

}