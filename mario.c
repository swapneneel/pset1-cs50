#include <cs50.h>
#include <stdio.h>

int height;
int main(void)
{

    //taking inpu with dialog & condition
    printf("Height of pyramid: ");

    do
    {
        height = get_int();
        //prompt to retry
        if (height > 23 || height < 0)
        {
            printf("Retry: ");
        }
    } while (height > 23 || height < 0);


     for(int i=0; i < height; i++){
         //printing spaces
         for(int j=0; j < (height -i) - 1; j++){
             printf(" ");
         }

         //printing spaces
         for (int k = 0; k <= i; k++)
         {
             printf("#");
         }

         //space btwn 2nd half of pyramid
         printf("  ");

         //printing elements for next half
         for (int l = 0; l <= i; l++)
         {
             printf("#");
         }

         //going to next line
         printf("\n");
     }

}