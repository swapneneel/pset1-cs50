#include <cs50.h>
#include <stdio.h>
#include <math.h>

int main(void)
{
    //taking appropriate input
    float change_input;

    do
    {
        change_input = get_float("Change owed: ");
    }
    while (change_input < 0);

    //converting user input to int
    float change_conv = change_input * 100;
    int change = round(change_conv);//rounding off


    //calculating no of coins least
    int n = 0;
    while (change >= 25) //use quarters
    {
        n++;
        change = change - 25;
    }

    while (10 <= change) //use dims
    {
        n++;
        change = change - 10;
    }

    while (5 <= change) //use nickels
    {
        n++;
        change = change - 5;
    }

    while (0 < change) //use pennies
    {
        n++;
        change = change - 1;
    }

    printf("No of coins used is %i\n", n);
}